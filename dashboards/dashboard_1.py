import os
import dash
import dash_core_components as dcc
import dash_html_components as html

from app import app

def generte_plot():
    return dcc.Graph(
            figure={
                'data': [
                    {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
                    {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
                ],
                'layout': {
                    'title': 'Dash Data Visualization'
                }
            }
        )

layout = html.Div(
[
    html.Nav(
        html.Div(
            [
                html.A('Option1', className="nav-item nav-link", href='https://www.google.ch'),
                html.A('Option2', className="nav-item nav-link", href='https://www.google.ch'),
                html.A('Option3', className="nav-item nav-link", href='https://www.google.ch'),
            ],
            className='navbar-nav'
        ),
        className="navbar sticky-top navbar-expand-lg navbar-dark bg-dark"
    ),
    html.Div(
        [
            html.H1('Welcome to Dashboard 1'),
            html.P('Dash: A web application framework for Python.'),
        ]
    ),
    html.Div(
        [
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
        ],
        className='bottom30 row',
    ),
    html.Div(
        [
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
        ],
        className='bottom30 row',
    ),
    html.Div(
        [
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
            html.Div(generte_plot(), className='col-xl'),
        ],
        className='bottom30 row',
    ),
    html.Div(
        [
            html.Div(generte_plot(), className='col-xl'),
        ],
        className='bottom30 row',
    ),
],
className='dalmo jumbotron container'
)
